# README #

This is a simple python script along with a directory containing all the necessary files to setup a backbone folder.

### What is this repository for? ###

* Initializing a directory with all the requirements of backbone
* Setting up the project directory structure in an organized way
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You just need python installed in your system use this command on a linux machine
  ```sudo apt-get install python2.7```
* Paste the script and the directory in your project folder
* Use the following code to allow executable permissions to the script
  ```chmod -x init-backbone.py```
* Setup your project using
  ```./init-backbone.py <YOUR-DIRECTORY-NAME>```