#!/usr/bin/env python

# this script initializes a backbone directory with 
# everything necessary

import os
import sys
from shutil import copyfile

def main():
	if(len(sys.argv)<=1):
		print "Please specify directory name !!!"
		sys.exit()
	else:
		# fetch the directory name
		dir_name = sys.argv[1]

		# setup all the directories
		print "setting up directories...",
		os.mkdir(dir_name)
		os.mkdir(dir_name+'/'+'css')
		os.mkdir(dir_name+'/'+'js')
		os.mkdir(dir_name+'/'+'js/'+'lib')
		os.mkdir(dir_name+'/'+'js/'+'models')
		os.mkdir(dir_name+'/'+'js/'+'views')
		os.mkdir(dir_name+'/'+'js/'+'collections')
		os.mkdir(dir_name+'/'+'js/'+'routers')
		print "done"

		# copy the essential files
		print "setting up javascript libraries...",
		copyfile('backbone/backbone-min.js',dir_name+'/js/lib/backbone-min.js')
		copyfile('backbone/underscore-min.js',dir_name+'/js/lib/underscore-min.js')
		copyfile('backbone/jquery-1.9.1.min.js',dir_name+'/js/lib/jquery-1.9.1.min.js')
		copyfile('backbone/handlebars.min.js',dir_name+'/js/lib/handlebars.min.js')
		print "done"

		print "setting up index.html...",
		copyfile('backbone/index.html',dir_name+'/index.html')
		print "done"

		print "Finished setting up your backbone project."

if __name__ == "__main__":
	main()
